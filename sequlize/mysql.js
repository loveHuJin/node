const { Sequelize } = require('sequelize');
const db = new Sequelize('db01', 'root', '123456', {
    dialect: 'mysql',    //数据库类型
    host: '192.168.1.101',   //主机地址
    port: "3306",
    pool: {      //连接池设置
        max: 5,  //最大连接数
        idle: 30000,
        acquire: 60000
    },
    dialectOptions:{
        charset:'utf8mb4',  //字符集
        collate:'utf8mb4_unicode_ci'
    },
    define: {   //模型设置
        freezeTableName:true,    //自定义表面，不设置会自动将表名转为复数形式
        timestamps:false    //自动生成更新时间、创建时间字段：updatedAt,createdAt
    }
    
})


async function main () {
    try {
        await db.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }

}

main () 


